/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

#include "d-bus.h"
#include "history.h"

#define DBUS_NAME "org.freedesktop.Dozed"
#define DBUS_PATH "/org/freedesktop/Dozed"

#define SYSTEMD_DBUS_NAME       "org.freedesktop.login1"
#define SYSTEMD_DBUS_PATH       "/org/freedesktop/login1"
#define SYSTEMD_DBUS_INTERFACE  "org.freedesktop.login1.Manager"

#define UPOWER_DBUS_NAME       "org.freedesktop.UPower"
#define UPOWER_DBUS_PATH       "/org/freedesktop/UPower/devices/DisplayDevice"
#define UPOWER_DBUS_INTERFACE  "org.freedesktop.UPower.Device"

#define REGISTER_TIMEOUT        2500
#define MAINTENANCE_NET_TIMEOUT 15000
#define MAINTENANCE_TIMEOUT     60000

struct _DozedBusPrivate {
    RtcAlarms *rtc_alarms;
    History *history;
    GDBusConnection *connection;
    GDBusNodeInfo *introspection_data;
    guint owner_id;
    GDBusProxy *logind_proxy;
    GDBusProxy *upower_proxy;
    guint maintenance_window_id;
    guint maintenance_count;
    guint maintenance_timeout_id;
    guint network_timeout_id;
    guint register_timeout_id;
    gboolean waiting_network;
};

G_DEFINE_TYPE_WITH_CODE (DozedBus, dozed_bus, G_TYPE_OBJECT,
    G_ADD_PRIVATE (DozedBus))

enum {
    PROP_0,
    PROP_RTC_ALARMS,
    PROP_HISTORY
};

static void
add_battery_level_to_history (DozedBus *self)
{
    g_autoptr(GVariant) variant;

    variant = g_dbus_proxy_get_cached_property (
        self->priv->upower_proxy,
        "Percentage"
    );

    if (variant != NULL) {
        g_autofree gchar *percentage;

        percentage = g_strdup_printf ("%f", g_variant_get_double (variant));

        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_SYSTEM,
            HISTORY_EVENT_BATTERY,
            percentage,
            NULL
        );
    }
}

static void
maintenance_started (gpointer user_data)
{
    DozedBus *self = user_data;

    g_message ("maintenance started");

    history_add_event (
        HISTORY (self->priv->history),
        HISTORY_EVENT_TYPE_MAINTENANCE,
        HISTORY_EVENT_STARTED,
        NULL
    );

    g_dbus_connection_emit_signal (
        self->priv->connection,
        NULL,
        DBUS_PATH,
        DBUS_NAME,
        "MaintenanceStarted",
        NULL,
        NULL
    );
}

static void
maintenance_finished (gpointer user_data)
{
    DozedBus *self = user_data;

    self->priv->maintenance_count = 0;

    g_message ("maintenance finished");

    history_add_event (
        HISTORY (self->priv->history),
        HISTORY_EVENT_TYPE_MAINTENANCE,
        HISTORY_EVENT_FINISHED,
        NULL
    );

    g_dbus_connection_emit_signal (
        self->priv->connection,
        NULL,
        DBUS_PATH,
        DBUS_NAME,
        "MaintenanceFinished",
        NULL,
        NULL
    );
}

static gboolean
register_timeout_cb (gpointer user_data)
{
    DozedBus *self = user_data;

    self->priv->register_timeout_id = 0;

    maintenance_started (user_data);

    if (self->priv->maintenance_count == 0) {
        maintenance_finished (user_data);
    }

    return FALSE;
}

static gboolean
check_maintenance_cb(gpointer user_data)
{
    DozedBus *self = user_data;

    self->priv->maintenance_timeout_id = 0;
    maintenance_finished (user_data);

    return FALSE;
}

static gboolean
check_network_maintenance_cb(gpointer user_data)
{
    DozedBus *self = user_data;
    GNetworkMonitor *network_monitor = g_network_monitor_get_default();

    self->priv->network_timeout_id = 0;

    if (g_network_monitor_get_network_available (network_monitor) &&
            self->priv->maintenance_timeout_id == 0) {
        self->priv->maintenance_timeout_id =
                g_timeout_add (
                    MAINTENANCE_TIMEOUT - MAINTENANCE_NET_TIMEOUT,
                    (GSourceFunc) check_maintenance_cb,
                    self);
    } else if (self->priv->maintenance_timeout_id == 0) {
        maintenance_finished (user_data);
    }

    return FALSE;
}

static void
handle_method_call (GDBusConnection *connection,
                    const gchar *sender,
                    const gchar *object_path,
                    const gchar *interface_name,
                    const gchar *method_name,
                    GVariant *parameters,
                    GDBusMethodInvocation *invocation,
                    gpointer user_data)
{
    DozedBus *self = user_data;

    if (g_strcmp0 (method_name, "AddAlarmTimeout") == 0) {
        gint64 alarm_id;
        guint64 seconds;
        const gchar* app_id;
        g_autofree gchar *alarm_id_str;

        g_variant_get (parameters, "(&st)", &app_id, &seconds);

        g_message (
            "%s adding alarm in %ld seconds",
            app_id,
            seconds
        );

        alarm_id = rtc_alarms_timeout (self->priv->rtc_alarms, app_id, seconds);
        alarm_id_str = g_strdup_printf ("%ld", alarm_id);

        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_ALARM,
            HISTORY_EVENT_ADD,
            app_id,
            alarm_id_str,
            NULL
        );

        g_dbus_method_invocation_return_value (
            invocation,
            g_variant_new ("(x)", alarm_id)
        );
    } else if (g_strcmp0 (method_name, "AddAlarmAt") == 0) {
        gint64 alarm_id;
        gint64 timestamp;
        const gchar* app_id;
        g_autofree gchar *alarm_id_str;

        g_variant_get (parameters, "(&sx)", &app_id, &timestamp);

        g_message (
            "%s adding alarm at %ld timestamp",
            app_id,
            timestamp
        );

        alarm_id = rtc_alarms_add (self->priv->rtc_alarms, app_id, timestamp);
        alarm_id_str = g_strdup_printf ("%ld", alarm_id);

        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_ALARM,
            HISTORY_EVENT_ADD,
            app_id,
            alarm_id_str,
            NULL
        );

        g_dbus_method_invocation_return_value (
            invocation,
            g_variant_new ("(x)", alarm_id)
        );
    } else if (g_strcmp0 (method_name, "RemoveAlarm") == 0) {
        gint64 alarm_id;
        const gchar* app_id;
        g_autofree gchar *alarm_id_str;
        gboolean deleted;

        g_variant_get (parameters, "(&sx)", &app_id, &alarm_id);
        deleted = rtc_alarms_del (self->priv->rtc_alarms, app_id, alarm_id);

        g_message (
            "%s removing alarm %ld",
            app_id,
            alarm_id
        );

        alarm_id_str = g_strdup_printf ("%ld", alarm_id);
        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_ALARM,
            HISTORY_EVENT_DEL,
            app_id,
            alarm_id_str,
            NULL
        );

        g_dbus_method_invocation_return_value (
            invocation,
            g_variant_new ("(b)", deleted)
        );
    } else if (g_strcmp0 (method_name, "RegisterMaintenanceWindow") == 0) {
        const gchar* app_id;
        gboolean waiting_network;

        // No more registration
        if (self->priv->register_timeout_id == 0) {
            g_debug ("Ignoring RegisterMaintenanceWindow, too late");
            g_dbus_method_invocation_return_value (
                invocation,
                g_variant_new ("(x)", 0)
            );
            return;
        }

        g_variant_get (parameters, "(&sb)", &app_id, &waiting_network);

        if (waiting_network)
            self->priv->waiting_network = waiting_network;

        self->priv->maintenance_count += 1;

        if (self->priv->waiting_network) {
            g_clear_handle_id (&self->priv->network_timeout_id, g_source_remove);
            self->priv->network_timeout_id =
                g_timeout_add (
                    MAINTENANCE_NET_TIMEOUT,
                    (GSourceFunc) check_network_maintenance_cb,
                    self);
        } else {
            g_clear_handle_id (&self->priv->maintenance_timeout_id, g_source_remove);
            self->priv->maintenance_timeout_id =
                g_timeout_add (
                    MAINTENANCE_TIMEOUT,
                    (GSourceFunc) check_maintenance_cb,
                    self);
        }

        g_message (
            "%s maintenance started: %d",
            app_id,
            self->priv->maintenance_count
        );

        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_MAINTENANCE,
            HISTORY_EVENT_REGISTERED,
            app_id,
            NULL
        );

        g_dbus_method_invocation_return_value (
            invocation,
            g_variant_new ("(x)", self->priv->maintenance_window_id)
        );
    } else if (g_strcmp0 (method_name, "ReleaseMaintenanceWindow") == 0) {
        const gchar* app_id;
        guint maintenance_window_id;

        g_variant_get (parameters, "(&sx)", &app_id, &maintenance_window_id);

        if (self->priv->maintenance_count == 0 ||
                maintenance_window_id == 0 ||
                maintenance_window_id != self->priv->maintenance_window_id) {
            return;
        }

        self->priv->maintenance_count -= 1;

        g_message (
            "%s maintenance stopped: %d",
            app_id,
            self->priv->maintenance_count
        );

        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_MAINTENANCE,
            HISTORY_EVENT_RELEASED,
            app_id,
            NULL
        );

        /* Maintenance can't be finished if register window active */
        if (self->priv->register_timeout_id == 0 &&
                self->priv->maintenance_timeout_id != 0 &&
                self->priv->maintenance_count == 0) {
            g_clear_handle_id (&self->priv->register_timeout_id, g_source_remove);
            g_clear_handle_id (&self->priv->maintenance_timeout_id, g_source_remove);
            g_clear_handle_id (&self->priv->network_timeout_id, g_source_remove);
            maintenance_finished (self);
        }
    }
}

static void
on_logind_proxy_signal (GDBusProxy  *proxy,
                        const gchar *sender_name,
                        const gchar *signal_name,
                        GVariant    *parameters,
                        gpointer     user_data)
{
        DozedBus *self = user_data;
        gboolean is_about_to_suspend;

        if (g_strcmp0 (signal_name, "PrepareForSleep") != 0)
                return;

        g_clear_handle_id (&self->priv->maintenance_timeout_id, g_source_remove);
        g_clear_handle_id (&self->priv->network_timeout_id, g_source_remove);
        self->priv->maintenance_count = 0;
        self->priv->waiting_network = FALSE;

        g_variant_get (parameters, "(b)", &is_about_to_suspend);
        if (is_about_to_suspend) {
            g_message ("System suspending");
            history_add_event (
                HISTORY (self->priv->history),
                HISTORY_EVENT_TYPE_SYSTEM,
                HISTORY_EVENT_SUSPEND,
                NULL
            );
        } else {
            GDateTime *datetime = g_date_time_new_now_utc ();

            g_message ("System resuming");

            history_add_event (
                HISTORY (self->priv->history),
                HISTORY_EVENT_TYPE_SYSTEM,
                HISTORY_EVENT_RESUME,
                NULL
            );

            rtc_alarms_force_ring (self->priv->rtc_alarms);
            self->priv->maintenance_window_id = g_date_time_to_unix (datetime);
            g_date_time_unref (datetime);
            self->priv->register_timeout_id =
                g_timeout_add (
                    REGISTER_TIMEOUT,
                    (GSourceFunc) register_timeout_cb,
                    self);
        }
}

static void
on_upower_proxy_properties (GDBusProxy* proxy,
                            GVariant* changed_properties,
                            char** invalidated_properties,
                            gpointer user_data)
{
    DozedBus *self = user_data;
    char *property;
    GVariantIter i;

    g_variant_iter_init (&i, changed_properties);
    while (g_variant_iter_next (&i, "{&sv}", &property, NULL)) {
        if (g_strcmp0 (property, "Percentage") == 0) {
            add_battery_level_to_history (self);
        }
    }
}


static void
on_alarm_ringed (GObject *Object,
                 gint64   alarm_id,
                 gboolean ringed,
                 gpointer user_data)
{
    DozedBus *self = user_data;
    g_autofree gchar *alarm_id_str;

    g_dbus_connection_emit_signal (
        self->priv->connection,
        NULL,
        DBUS_PATH,
        DBUS_NAME,
        "AlarmRinged",
        g_variant_new ("(xb)", alarm_id, ringed),
        NULL
    );

    alarm_id_str = g_strdup_printf ("%ld", alarm_id);

    if (ringed)
        history_add_event (
            HISTORY (self->priv->history),
            HISTORY_EVENT_TYPE_ALARM,
            HISTORY_EVENT_RINGED,
            alarm_id_str,
            NULL
        );
}

static const GDBusInterfaceVTable interface_vtable = {
    handle_method_call,
    NULL,
    NULL
};

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar *name,
                 gpointer user_data)
{
    DozedBus *self = user_data;
    guint registration_id;

    registration_id = g_dbus_connection_register_object (
        connection,
        DBUS_PATH,
        self->priv->introspection_data->interfaces[0],
        &interface_vtable,
        user_data,
        NULL,
        NULL
    );

    self->priv->connection = g_object_ref (connection);

    g_assert (registration_id > 0);
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
    DozedBus *self = user_data;

    g_signal_connect(
        self->priv->rtc_alarms,
        "ringed",
        G_CALLBACK(on_alarm_ringed),
        self
    );
}


static void
on_name_lost (GDBusConnection *connection,
              const gchar *name,
              gpointer user_data)
{
    g_printerr ("Cannot own D-Bus name. Verify installation: %s\n", name);
    exit (1);
}

static void
dozed_bus_set_property (GObject *object,
                        guint property_id,
                        const GValue *value,
                        GParamSpec *pspec)
{
    DozedBus *self = DOZED_BUS (object);

    switch (property_id) {
        case PROP_RTC_ALARMS:
            self->priv->rtc_alarms = g_value_get_object (value);
            return;
        case PROP_HISTORY:
            self->priv->history = g_value_get_object (value);
            return;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
dozed_bus_get_property (GObject *object,
                        guint property_id,
                        GValue *value,
                        GParamSpec *pspec)
{
    DozedBus *self = DOZED_BUS (object);

    switch (property_id) {
        case PROP_RTC_ALARMS:
            g_value_set_object (
                value, self->priv->rtc_alarms);
            return;
        case PROP_HISTORY:
            g_value_set_object (
                value, self->priv->history);
            return;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
dozed_bus_dispose (GObject *dozed_bus)
{
    DozedBus *self = DOZED_BUS (dozed_bus);

    if (self->priv->owner_id != 0) {
        g_bus_unown_name (self->priv->owner_id);
    }

    g_clear_pointer (&self->priv->introspection_data, g_dbus_node_info_unref);
    g_clear_object (&self->priv->connection);
    g_clear_object (&self->priv->logind_proxy);
    g_clear_object (&self->priv->upower_proxy);
    g_object_unref (&self->priv->rtc_alarms);

    g_free (self->priv);

    G_OBJECT_CLASS (dozed_bus_parent_class)->dispose (dozed_bus);
}

static void
dozed_bus_class_init (DozedBusClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);
    object_class->set_property = dozed_bus_set_property;
    object_class->get_property = dozed_bus_get_property;
    object_class->dispose = dozed_bus_dispose;

    g_object_class_install_property (
        object_class,
        PROP_RTC_ALARMS,
        g_param_spec_object (
            "rtc-alarms",
            "RTC alarms",
            "An RTC alarms object allowing to communicate with Linux RTC device",
            TYPE_RTC_ALARMS,
            G_PARAM_WRITABLE|
            G_PARAM_CONSTRUCT_ONLY|
            G_PARAM_STATIC_STRINGS
        )
    );
    g_object_class_install_property (
        object_class,
        PROP_HISTORY,
        g_param_spec_object (
            "history",
            "Dozing history",
            "A history object allowing to write events file",
            TYPE_HISTORY,
            G_PARAM_WRITABLE|
            G_PARAM_CONSTRUCT_ONLY|
            G_PARAM_STATIC_STRINGS
        )
    );

}

static void
dozed_bus_init (DozedBus *self)
{
    GBytes *bytes;

    self->priv = dozed_bus_get_instance_private (self);

    bytes = g_resources_lookup_data (
        "/org/freedesktop/Dozed/org.freedesktop.Dozed.xml",
        G_RESOURCE_LOOKUP_FLAGS_NONE,
        NULL
    );
    self->priv->introspection_data = g_dbus_node_info_new_for_xml (
        g_bytes_get_data (bytes, NULL),
        NULL
    );
    g_bytes_unref (bytes);

    g_assert (self->priv->introspection_data != NULL);

    self->priv->owner_id = g_bus_own_name (
        G_BUS_TYPE_SYSTEM,
        DBUS_NAME,
        G_BUS_NAME_OWNER_FLAGS_NONE,
        on_bus_acquired,
        on_name_acquired,
        on_name_lost,
        self,
        NULL
    );

    self->priv->logind_proxy =
                g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                               0,
                                               NULL,
                                               SYSTEMD_DBUS_NAME,
                                               SYSTEMD_DBUS_PATH,
                                               SYSTEMD_DBUS_INTERFACE,
                                               NULL,
                                               NULL);
    if (self->priv->logind_proxy == NULL) {
            g_warning ("No systemd (logind) support");
    } else {
        g_signal_connect (self->priv->logind_proxy, "g-signal",
                          G_CALLBACK (on_logind_proxy_signal),
                          self);
    }

    self->priv->upower_proxy =
                g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                               0,
                                               NULL,
                                               UPOWER_DBUS_NAME,
                                               UPOWER_DBUS_PATH,
                                               UPOWER_DBUS_INTERFACE,
                                               NULL,
                                               NULL);
    if (self->priv->upower_proxy == NULL) {
            g_warning ("No UPower support");
    } else {
        add_battery_level_to_history (self);
        g_signal_connect (self->priv->upower_proxy, "g-properties-changed",
                          G_CALLBACK (on_upower_proxy_properties),
                          self);
    }

    self->priv->maintenance_window_id = 0;
    self->priv->maintenance_timeout_id = 0;
    self->priv->network_timeout_id = 0;
    self->priv->register_timeout_id = 0;
}

/**
 * dozed_bus_new:
 * @rtc_alarms: a #RtcAlarms
 *
 * Creates a new #DozedBus
 *
 * Returns: (transfer full): a new #DozedBus
 *
 **/
GObject *
dozed_bus_new (RtcAlarms *rtc_alarms, History *history)
{
    GObject *dozed_bus;

    dozed_bus = g_object_new (
        TYPE_DOZED_BUS,
        "rtc-alarms",
        rtc_alarms,
        "history",
        history,
        NULL
    );

    return dozed_bus;
}
