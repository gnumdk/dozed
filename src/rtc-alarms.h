/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

#ifndef RTC_H
#define RTC_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <gio/gunixinputstream.h>

#include <linux/rtc.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define TYPE_RTC_ALARMS \
    (rtc_alarms_get_type ())
#define RTC_ALARMS(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST \
    ((obj), TYPE_RTC_ALARMS, RtcAlarms))
#define RTC_ALARMS_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_CAST \
    ((cls), TYPE_RTC_ALARMS, RtcAlarmsClass))
#define IS_RTC_ALARMS(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE \
    ((obj), TYPE_RTC_ALARMS))
#define IS_RTC_ALARMS_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_TYPE \
    ((cls), TYPE_RTC_ALARMS))
#define RTC_ALARMS_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS \
    ((obj), TYPE_RTC_ALARMS, RtcAlarmsClass))

G_BEGIN_DECLS

typedef struct _RtcAlarms RtcAlarms;
typedef struct _RtcAlarmsClass RtcAlarmsClass;
typedef struct _RtcAlarmsPrivate RtcAlarmsPrivate;

struct _RtcAlarms {
    GObject parent;
    RtcAlarmsPrivate *priv;
};

struct _RtcAlarmsClass {
    GObjectClass parent_class;
};

GType           rtc_alarms_get_type      (void) G_GNUC_CONST;

GObject*        rtc_alarms_new           (void);
gint64          rtc_alarms_add           (RtcAlarms *self,
                                          const gchar *app_id,
                                          const gint64 timestamp);
gboolean        rtc_alarms_del           (RtcAlarms *self,
                                          const gchar *app_id,
                                          const gint64 alarm_id);
gint64          rtc_alarms_timeout       (RtcAlarms *self,
                                          const gchar *app_id,
                                          const guint64 seconds);
void            rtc_alarms_force_ring    (RtcAlarms *self);

G_END_DECLS

#endif

