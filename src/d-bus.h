/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

#ifndef DBUS_H
#define DBUS_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <stdlib.h>

#include "history.h"
#include "rtc-alarms.h"

#define TYPE_DOZED_BUS (dozed_bus_get_type ())

#define DOZED_BUS(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST \
    ((obj), TYPE_DOZED_BUS, DozedBus))
#define DOZED_BUS_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_CAST \
    ((cls), TYPE_DOZED_BUS, DozedBusClass))
#define IS_DOZED_BUS(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE \
    ((obj), TYPE_DOZED_BUS))
#define IS_DOZED_BUS_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_TYPE \
    ((cls), TYPE_DOZED_BUS))
#define DOZED_BUS_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS \
    ((obj), TYPE_DOZED_BUS, DozedBusClass))

G_BEGIN_DECLS

typedef struct _DozedBus DozedBus;
typedef struct _DozedBusClass DozedBusClass;
typedef struct _DozedBusPrivate DozedBusPrivate;

struct _DozedBus {
    GObject parent;
    DozedBusPrivate *priv;
};

struct _DozedBusClass {
    GObjectClass parent_class;
};

GType       dozed_bus_get_type      (void) G_GNUC_CONST;

GObject*    dozed_bus_new           (RtcAlarms *rtc_alarms,
                                     History *history);

G_END_DECLS

#endif
