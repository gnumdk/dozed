/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

#include "config.h"

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>

#include "d-bus.h"
#include "rtc-alarms.h"

gint
main (gint argc, gchar * argv[])
{
    GMainLoop *loop;
    GObject *dozed_bus;
    GObject *rtc_alarms;
    GObject *history;
    GResource *resource;
    g_autoptr (GOptionContext) context = NULL;
    g_autoptr (GError) error = NULL;
    gboolean version = FALSE;
    GOptionEntry main_entries[] = {
        {"version", 0, 0, G_OPTION_ARG_NONE, &version, "Show version"},
        {NULL}
    };

    context = g_option_context_new ("Doze support");
    g_option_context_add_main_entries (context, main_entries, NULL);

    if (!g_option_context_parse (context, &argc, &argv, &error)) {
        g_printerr ("%s\n", error->message);
        return EXIT_FAILURE;
    }

    if (version) {
        g_printerr ("%s\n", PACKAGE_VERSION);
        return EXIT_SUCCESS;
    }

    resource = g_resource_load (DOZED_RESOURCES, NULL);
    g_resources_register (resource);

    rtc_alarms = rtc_alarms_new ();
    history = history_new ();
    dozed_bus = dozed_bus_new (
        RTC_ALARMS (rtc_alarms),
        HISTORY (history)
    );

    loop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (loop);

    g_clear_pointer (&loop, g_main_loop_unref);
    g_clear_object (&dozed_bus);
    g_clear_object (&history);
    g_clear_object (&rtc_alarms);
    g_clear_object (&resource);

    return EXIT_SUCCESS;
}
