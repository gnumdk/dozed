/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */
#include "rtc-alarms.h"

#define RTC_DEVICE "/dev/rtc0"

struct _RtcAlarmsPrivate {
    GArray *alarms;
    GCancellable *cancellable;
    gboolean locked;
};

struct RtcAlarm {
    const gchar *app_id;
    gint64 alarm_id;
};

/* signals */
enum
{
    RINGED,
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

G_DEFINE_TYPE_WITH_CODE (
    RtcAlarms,
    rtc_alarms,
    G_TYPE_OBJECT,
    G_ADD_PRIVATE (RtcAlarms)
)

static gint
cmp_alarm (gconstpointer a,
           gconstpointer b)
{
    const struct RtcAlarm *_a = *(struct RtcAlarm **) a;
    const struct RtcAlarm *_b = *(struct RtcAlarm **) b;
    return _a->alarm_id - _b->alarm_id;
}

static gint64
rtc_alarms_get_timestamp (RtcAlarms *self)
{
    struct rtc_time rtc_tm;
    int fd, retval;
    GDateTime *datetime;
    guint64 timestamp = 0;

    fd = open (RTC_DEVICE, O_RDONLY);

    if (fd ==  -1) {
        g_warning ("Could not open %s", RTC_DEVICE);
        goto close;
    }

    retval = ioctl (fd, RTC_RD_TIME, &rtc_tm);
    if (retval == -1) {
        g_warning ("Could not read RTC time");
        goto out;
    }

    datetime = g_date_time_new_utc (
        rtc_tm.tm_year + 1900,
        rtc_tm.tm_mon + 1,
        rtc_tm.tm_mday,
        rtc_tm.tm_hour,
        rtc_tm.tm_min,
        rtc_tm.tm_sec
    );

    timestamp = g_date_time_to_unix (datetime);

    g_date_time_unref (datetime);

close:
    close (fd);
out:
    return timestamp;
}

static gint64
get_drift (RtcAlarms *self)
{
    GDateTime *datetime;
    gint64 hw_timestamp, sw_timestamp;
    static gint64 drift = 0;

    if (self->priv->locked) {
        return drift;
    }

    datetime = g_date_time_new_now_utc ();
    sw_timestamp = g_date_time_to_unix (datetime);

    hw_timestamp = rtc_alarms_get_timestamp (self);

    drift = hw_timestamp - sw_timestamp;

    g_date_time_unref (datetime);

    return drift;
}

static void
wait_alarm_ring_thread (GTask *task,
                        gpointer source_object,
                        gpointer task_data,
                        GCancellable *cancellable)
{
    GInputStream *stream;
    gint retval, fd;
    guint64 data;

    fd = open (RTC_DEVICE, O_RDONLY);

    if (fd ==  -1) {
        g_warning ("Could not open: %s", RTC_DEVICE);
        g_task_return_boolean(task, TRUE);
        goto out;
    }

    retval = ioctl(fd, RTC_AIE_ON, 0);

    if (retval == -1) {
        g_warning ("Could not enable the alarm interrupt");
        g_task_return_boolean(task, TRUE);
        goto close;
    }

    stream = g_unix_input_stream_new (fd, FALSE);
    retval = g_input_stream_read (stream, &data, 8, cancellable, NULL);
    if (g_cancellable_is_cancelled (cancellable)) {
        g_message ("Stop waiting for alarm");
        g_task_return_boolean(task, FALSE);
        goto close_stream;
    } else if (retval == -1) {
        g_warning ("Could not wait for alarm");
    }

    retval = ioctl(fd, RTC_AIE_OFF, 0);
    if (retval == -1) {
        g_warning ("Could not disable the alarm interrupt");
    }

    g_task_return_boolean(task, TRUE);

close_stream:
    g_input_stream_close (stream, NULL, NULL);
close:
    close (fd);
out:
}

static void
on_alarm_ringed (GObject      *source_object,
                 GAsyncResult *res,
                 gpointer      user_data)
{
    RtcAlarms *self = RTC_ALARMS (source_object);
    gint64 timestamp_id = (gint64) user_data;
    gboolean ringed;

    self->priv->locked = FALSE;

    ringed = g_task_propagate_boolean(G_TASK(res), NULL);

    g_message ("Alarm ringed:  %ld, %d", timestamp_id, ringed);

    g_signal_emit(self, signals[RINGED], 0, timestamp_id, ringed);
    if (ringed) {
        rtc_alarms_del (self, NULL, timestamp_id);
    }
}

static void
wait_alarm_ring_async (RtcAlarms *self,
                       gint64 timestamp)
{
    GTask *task;

    self->priv->cancellable = g_cancellable_new ();
    self->priv->locked = TRUE;

    task = g_task_new (
        self, self->priv->cancellable, on_alarm_ringed, (gpointer) timestamp
    );
    g_task_run_in_thread(task, wait_alarm_ring_thread);

    g_object_unref(task);
}

static gboolean
set_rtc (RtcAlarms *self, const gint64 timestamp)
{
    int fd, retval;
    struct tm tm;
    struct rtc_wkalrm wake = { 0 };

    g_debug ("set_rtc(): %ld", timestamp);

    gmtime_r((time_t *) &timestamp, &tm);
    wake.time.tm_sec = tm.tm_sec;
    wake.time.tm_min = tm.tm_min;
    wake.time.tm_hour = tm.tm_hour;
    wake.time.tm_mday = tm.tm_mday;
    wake.time.tm_mon = tm.tm_mon;
    wake.time.tm_year = tm.tm_year;
    wake.time.tm_wday = -1;
    wake.time.tm_yday = -1;
    wake.time.tm_isdst = -1;
    wake.enabled = 1;

    fd = open (RTC_DEVICE, O_RDONLY);

    if (fd ==  -1) {
        g_warning ("Could not open: %s", RTC_DEVICE);
        return FALSE;
    }

    retval = ioctl (fd, RTC_WKALM_SET, &wake);
    if (retval < 0) {
        g_warning ("Could not set an alarm");
    }

    close(fd);

    return retval >= 0;
}

static gboolean
schedule_alarms (gpointer user_data)
{
    RtcAlarms *self = user_data;
    struct RtcAlarm *rtc_alarm;

    if (self->priv->locked) {
        g_cancellable_cancel (self->priv->cancellable);
        g_idle_add (schedule_alarms, user_data);
    } else if (self->priv->alarms->len > 0) {
        rtc_alarm = g_array_index (self->priv->alarms, struct RtcAlarm *, 0);
        set_rtc (self, 0);
        if (set_rtc (self, rtc_alarm->alarm_id)) {
            wait_alarm_ring_async (self, rtc_alarm->alarm_id);
        } else {
            /* Unable to apply value, remove it */
            g_array_remove_index (self->priv->alarms, 0);
            g_free (rtc_alarm);
            g_idle_add (schedule_alarms, self);
        }
    } else {
        set_rtc (self, 0);
    }

    return G_SOURCE_REMOVE;
}

static void
rtc_alarms_dispose (GObject *rct_alarms)
{
    RtcAlarms *self = RTC_ALARMS (rct_alarms);

    g_array_free (self->priv->alarms, TRUE);
    g_clear_object (&self->priv->alarms);
    g_clear_object (&self->priv->cancellable);

    g_free (self->priv);

    G_OBJECT_CLASS (rtc_alarms_parent_class)->dispose (rct_alarms);
}

static void
rtc_alarms_class_init (RtcAlarmsClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);
    object_class->dispose = rtc_alarms_dispose;

    signals[RINGED] = g_signal_new (
        "ringed",
        G_OBJECT_CLASS_TYPE (object_class),
        G_SIGNAL_RUN_LAST,
        0,
        NULL, NULL, NULL,
        G_TYPE_NONE,
        2,
        G_TYPE_INT64,
        G_TYPE_BOOLEAN);
}

static void
rtc_alarms_init (RtcAlarms *self)
{
    self->priv = rtc_alarms_get_instance_private (self);

    self->priv->locked = FALSE;
    self->priv->alarms = g_array_new (FALSE, FALSE, sizeof (struct RtcAlarm *));

    get_drift (self);
}

/**
 * rtc_alarms_new:
 *
 * Creates a new #RtcAlarms
 *
 * Returns: (transfer full): a new #RtcAlarms
 *
 **/
GObject *
rtc_alarms_new (void)
{
    GObject *rtc_alarms;

    rtc_alarms = g_object_new (TYPE_RTC_ALARMS, NULL);

    return rtc_alarms;
}

/**
 * rtc_alarms_add:
 * @self: a #RtcAlarms
 * @app_id: application id as org.domain.Appname
 * @timestamp: timestamp to set
 *
 * Add a new alarm for timestamp
 *
 * Returns: a new timestamp id
 **/
gint64
rtc_alarms_add (RtcAlarms *self,
                const gchar* app_id,
                const gint64 timestamp)
{
    struct RtcAlarm *rtc_alarm;
    gint64 alarm_id, drift;

    drift = get_drift (self);

    /* alarm id is just timestamp synced to RTC clock */
    alarm_id = timestamp + drift;

    rtc_alarm = g_malloc (sizeof (struct RtcAlarm));
    if (!rtc_alarm) {
        g_error ("Can't allocate memory!");
        return 0;
    }
    rtc_alarm->app_id = g_strdup (app_id);
    rtc_alarm->alarm_id = alarm_id;

    g_array_append_val (self->priv->alarms, rtc_alarm);
    g_array_sort (self->priv->alarms, cmp_alarm);

    schedule_alarms (self);

    return alarm_id;
}

/**
 * rtc_alarms_del:
 * @self: a #RtcAlarms
 * @app_id: application id as org.domain.Appname
 * @alarm_id: alarm id to delete
 *
 * Delete an alarm with id
 *
 * Returns: TRUE if removed
 **/
gboolean
rtc_alarms_del (RtcAlarms *self,
                const gchar* app_id,
                const gint64 alarm_id)
{
    struct RtcAlarm *rtc_alarm;
    gboolean same_app_id;
    gboolean deleted = FALSE;

    for (int i = 0; i < self->priv->alarms->len; ++i) {
        rtc_alarm = g_array_index (self->priv->alarms, struct RtcAlarm *, i);
        same_app_id = app_id == NULL || g_strcmp0 (
            rtc_alarm->app_id, app_id
        ) == 0;
        if (same_app_id && rtc_alarm->alarm_id == alarm_id) {
            g_array_remove_index (self->priv->alarms, i);
            g_free (rtc_alarm);
            deleted = TRUE;
            break;
        }
    }

    schedule_alarms (self);

    return deleted;
}

/**
 * rtc_alarms_timeout:
 * @self: a #RtcAlarms
 * @app_id: application id as org.domain.Appname
 * @timeout: alarm timeout in seconds
 *
 * Add a new alarm ringing in timeout seconds
 *
 * Returns: a new timestamp id
 **/
gint64
rtc_alarms_timeout (RtcAlarms *self,
                    const gchar* app_id,
                    const guint64 seconds)
{
    struct RtcAlarm *rtc_alarm;
    gint64 alarm_id, rtc_timestamp;

    if (self->priv->locked) {
        GDateTime *datetime;
        gint64 drift;
        drift = get_drift (self);
        datetime = g_date_time_new_now_utc ();
        rtc_timestamp = g_date_time_to_unix (datetime) + drift;
        g_date_time_unref (datetime);
    } else {
        rtc_timestamp = rtc_alarms_get_timestamp (self);
    }

    alarm_id = rtc_timestamp + seconds;

    rtc_alarm = g_malloc (sizeof (struct RtcAlarm));
    if (!rtc_alarm) {
        g_error ("Can't allocate memory!");
        return 0;
    }
    rtc_alarm->app_id = g_strdup (app_id);
    rtc_alarm->alarm_id = alarm_id;
    g_array_append_val (self->priv->alarms, rtc_alarm);
    rtc_alarm = g_array_index (self->priv->alarms, struct RtcAlarm *, 0);
    g_array_sort (self->priv->alarms, cmp_alarm);

    schedule_alarms (self);

    return alarm_id;
}

/**
 * rtc_alarms_force_ring:
 * @self: a #RtcAlarms
 *
 * Force current alarm to ring
 *
 **/
void
rtc_alarms_force_ring (RtcAlarms *self)
{
    if (self->priv->cancellable != NULL) {
        g_timeout_add (250, (GSourceFunc) g_cancellable_cancel, self->priv->cancellable);
    }
}
