# Doze daemon

This DBus service allows to setup RTC alarms.

## Depends on

- `glib2`
- `meson`
- `ninja`

## Building from Git

```bash
$ meson builddir --prefix=/usr

# sudo ninja -C builddir install
```
